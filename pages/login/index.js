import { useState, useContext, useEffect } from "react";
import Router from "next/router";
import Link from "next/link";
import { Form, Button, Card, Row, Col } from "react-bootstrap";
import { PersonFill, KeyFill } from "react-bootstrap-icons";
import { GoogleLogin } from "react-google-login";
// import Facebook from '../../components/Facebook';
import Swal from "sweetalert2";
import { UserContext } from "../../UserContext";
import View from "../../components/View";
import AppHelper from "../../app-helper";

export default function index() {
  return (
    <Col xs={12} id="page-content-wrapper">
      <View title={"Login"}>
        <Row className="justify-content-center">
          <Col xs md="8">
            <LoginForm />
          </Col>
        </Row>
      </View>
    </Col>
  );
}

//Login thru email
const LoginForm = () => {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    //prevent redirection via form submission
    e.preventDefault();

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };

    fetch(`${AppHelper.API_URL}/users/login`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        console.log(data);

        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);

          Swal.fire({
            position: "center",
            icon: "success",
            title: "Welcome budgetarian!",
            showConfirmButton: false,
            timer: 2000,
          });
        } else {
          if (data.error === "does-not-exist") {
            Swal.fire("Authentication Failed", "User does not exists", "error");
          } else if (data.error === "incorrect-password") {
            Swal.fire(
              "Authentication Failed",
              "Password is incorrect",
              "error"
            );
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure, try alternative  login procedure",
              "error"
            );
          }
        }
      });
  }

  //Login thru google
  const authenticateGoogleToken = (response) => {
    console.log(response);

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        tokenId: response.tokenId,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        console.log(data);

        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);

          Swal.fire({
            position: "center",
            icon: "success",
            title: "Welcome budgetarian!",
            showConfirmButton: false,
            timer: 2000,
          });
        } else {
          if (data.error === "google-auth-error") {
            Swal.fire(
              "Google Auth Error",
              "Google authentication procedure failed.",
              "error"
            );
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure.",
              "error"
            );
          }
        }
      });
  };

  const retrieveUserDetails = (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    fetch(`${AppHelper.API_URL}/users/details`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          token: accessToken,
          ...data,
        });

        Router.push("/profile");
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Card style={{ backgroundColor: "#ddffbc", boxShadow: "0 0 10px #7b9481" }}>
      <Card.Body>
        <h2 className="p-4 text-center darkTextColor darkTextBorder">
          USER LOGIN
        </h2>
        <Form onSubmit={authenticate}>
          <Form.Group as={Row} controlId="userEmail" className="m-0">
            <Form.Label column md={2} className="text-center">
              <PersonFill color="#52734d" size={35} />
            </Form.Label>
            <Col md={10} className="p-0 m-auto">
              <Form.Control
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                autoComplete="off"
                placeholder="Email Address"
                required
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="password" className="m-0">
            <Form.Label column md={2} className="text-center">
              <KeyFill color="#52734d" size={35} />
            </Form.Label>
            <Col md={10} className="p-0 m-auto">
              <Form.Control
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Password"
                required
              />
            </Col>
          </Form.Group>

          {isActive ? (
            <Button type="submit" block className="mt-3" id="darkButton">
              Login
            </Button>
          ) : (
            <Button block disabled className="mt-3" id="darkButton">
              Fill out the email and/or password to login.
            </Button>
          )}

          <GoogleLogin
            clientId="639171538932-728vcbpdte416pmhdu3dog2i2ev10k0f.apps.googleusercontent.com"
            buttonText="Login with Google"
            onSuccess={authenticateGoogleToken}
            onFailure={authenticateGoogleToken}
            cookiePolicy={"single_host_origin"}
            className="w-50 text-center d-flex justify-content-center mt-2 mx-auto"
          />

          <Form.Group as={Row} className="mb-0">
            <Form.Label column lg={12} className="text-center">
              <p className="text-center darkTextColor">
                Don't have an account?{" "}
                <Link href="/register">
                  <strong>Sign up here.</strong>
                </Link>
              </p>
            </Form.Label>
          </Form.Group>
        </Form>
      </Card.Body>
    </Card>
  );
};
