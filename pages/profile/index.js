import React, { useContext, useEffect, useState } from "react";
import { Col, Row, Container, Card, Button } from "react-bootstrap";
import { UserContext } from "../../UserContext";
import { RecordContext } from "../../RecordContext";

export default function index() {
  const { user } = useContext(UserContext);
  const { records } = useContext(RecordContext);
  const [qoutes, setQuotes] = useState("");
  const [totalSavings, setTotalSavings] = useState([]);
  const [totalExpense, setTotalExpense] = useState([]);

  let randomNumber = Math.floor(Math.random() * 20);

  const getQuotes = () => {
    fetch("https://type.fit/api/quotes")
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        setQuotes(data[randomNumber]);
        console.log(data[randomNumber]);
      });
  };

  useEffect(() => {
    getQuotes();
    setQuotes({});
  }, []);

  const savings = (records) => {
    let setSavings = 0;
    records.forEach((record) => {
      if (record.categoryType === "Savings") {
        setSavings += parseInt(record.amount);
      }
    });
    return setSavings;
  };

  const expense = (records) => {
    let setExpense = 0;
    records.forEach((record) => {
      if (record.categoryType === "Expense") {
        setExpense += parseInt(record.amount);
      }
    });
    return setExpense;
  };

  useEffect(() => {
    setTotalSavings(savings(records));
    setTotalExpense(expense(records));
  }, [records]);

  return (
    <Col xs={10} id="page-content-wrapper">
      <Container>
        <Row>
          <Col md={4}>
            <Card
              style={{ boxShadow: "0 0 10px #7b9481" }}
              className="darkTextColor my-4 text-center"
            >
              <img
                width="150"
                height="150"
                style={{ borderRadius: "100px", margin: "10px auto" }}
                src={user.avatar}
              />
              <h5 id="nameProfile">
                {user.firstName} {user.lastName}
              </h5>
              <p className="textProfileSmall">{user.email}</p>
              <p className="textProfileSmall">{user.mobileNo}</p>
              <Button id="darkButton" className="mb-3">
                Edit Profile
              </Button>
            </Card>
            <Card
              style={{ boxShadow: "0 0 10px #7b9481" }}
              className="darkTextColor mt-4"
            >
              <p className="text-center mt-3 font-weight-bold">As of now</p>
              <Row>
                <Col col={6} className="ml-2 font-weight-bold text-center">
                  <p>Current Income: </p>
                  <p>Total Savings: </p>
                  <p>Total Expense</p>
                </Col>

                <Col col={6} className="font-weight-normal text-center">
                  <p>{records[0]?.transactionBalance}</p>
                  <p>{totalSavings}</p>
                  <p>{totalExpense}</p>
                </Col>
              </Row>
            </Card>
          </Col>

          <Col md={8}>
            <Card
              style={{ boxShadow: "0 0 10px #7b9481" }}
              className="darkTextColor mt-4 text-left"
            >
              <p className="pt-3 pl-4 font-weight-bolder">Food for thought:</p>
              <p className="text-center font-italic">"{qoutes.text}"</p>
              <p className="text-center"> - {qoutes.author || "Anonymous"}</p>
            </Card>
            <Card
              style={{ boxShadow: "0 0 10px #7b9481" }}
              className="darkTextColor mt-4 text-left p-5 text-center"
            >
              Not yet done - space for bar chart
            </Card>
          </Col>
        </Row>
      </Container>
    </Col>
  );
}
