import React, { useState, useEffect, useContext } from "react";
import Head from "next/head";
import { RecordContext } from "../../RecordContext";
import { Line } from "react-chartjs-2";
import { Col, Container } from "react-bootstrap";
import moment from "moment";

export default function index() {
  const { records } = useContext(RecordContext);
  const [months, setMonths] = useState([
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ]);
  const [monthlyIncome, setmonthlyIncome] = useState([]);
  useEffect(() => {
    if (records.length) {
      setmonthlyIncome(
        months.map((month) => {
          let income = 0;
          records.forEach((record) => {
            //moment syntax = moment(date).format(format)
            // console.log(moment(element.sale_date).format("MMMM"));
            if (
              moment(record.dateCreated).format("MMMM") === month &&
              record.categoryType === "Income"
            ) {
              income += parseInt(record.amount);
            }
          });
          return income;
        })
      );
    }
  }, [records]);

  const data = {
    labels: months,
    datasets: [
      {
        label: "Monthly Income",
        backgroundColor: "rgba(0, 105, 33, 0.49)",
        borderColor: "#358873",
        fill: true,
        borderWidth: 1,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        data: monthlyIncome,
      },
    ],
  };

  return (
    <React.Fragment>
      <Col xs={10} id="page-content-wrapper">
        <h1 className="text-center darkTextColor m-2">Monthly Income</h1>
        <Container>
          <Line data={data} />
        </Container>
      </Col>
    </React.Fragment>
  );
}
