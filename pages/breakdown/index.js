import React, { useState, useEffect, useContext } from "react";
import { colorRandomizer } from "../../app-helper";
import { RecordContext } from "../../RecordContext";
import { Pie } from "react-chartjs-2";
import { Col, Container, Row, Form } from "react-bootstrap";
import moment from "moment";
import { CategoryContext } from "../../CategoryContext";

export default function index() {
  const categoryTypeLists = ["Income", "Expense", "Savings"];
  let getDaysBetweenDates = function (startDate, endDate) {
    console.log(startDate);
    // return;
    var now = startDate.clone(),
      dates = [];

    while (now.isSameOrBefore(endDate)) {
      dates.push(now.format("MM/DD/YYYY"));
      now.add(1, "days");
    }
    return dates;
  };

  const { records } = useContext(RecordContext);
  const { categories } = useContext(CategoryContext);

  const [incomeData, setIncomeData] = useState();
  const [expenseData, setExpenseData] = useState();
  const [savingsData, setSavingsData] = useState();

  const [betweenDates, setBetweenDates] = useState("");
  const [startDate, setStartDate] = useState(moment().subtract(1, "m"));
  const [endDate, setEndDate] = useState(moment());

  const [bgColors, setBgColors] = useState([]);

  useEffect(() => {
    setBetweenDates(getDaysBetweenDates(moment(startDate), moment(endDate)));
  }, [startDate, endDate]);

  const labels = categories.map((category) => category.categoryName);

  const getSummary = (datas) => {
    console.log(datas);
    // return;
    const uniqueCategoryName = [
      ...new Set(datas.map((cat) => cat.categoryName)),
    ];

    console.log(uniqueCategoryName);

    const totalResult = uniqueCategoryName.map((data) => {
      let totalAmount = 0;
      betweenDates.map((day) => {
        datas.map((item) => {
          if (moment(data.dateCreated).format("MM/DD/YYYY") === day) {
            if (data === item.categoryName) {
              totalAmount += item.amount;
            }
          }
        });
      });

      return totalAmount;
    });

    return {
      categoryNames: uniqueCategoryName,
      totalResult,
    };

    console.log(totalResult);
  };

  useEffect(() => {
    console.log(labels);
    if (records.length) {
      const income = records.filter(
        (record) => record.categoryType === "Income"
      );

      const expense = records.filter(
        (record) => record.categoryType === "Expense"
      );

      const savings = records.filter(
        (record) => record.categoryType === "Savings"
      );

      // return totalAmount;
      setIncomeData(getSummary(income));
      setExpenseData(getSummary(expense));
      setSavingsData(getSummary(savings));
      setBgColors(income.map(() => `#${colorRandomizer()}`));
      setBgColors(savings.map(() => `#${colorRandomizer()}`));
      setBgColors(expense.map(() => `#${colorRandomizer()}`));
    }

    //   setBalance(
    //     betweenDates.map((day) => {
    //       let bal = [];
    //       records.forEach((record) => {
    //         //moment syntax = moment(date).format(format)
    //         // console.log(moment(element.sale_date).format("MMMM"));
    //         if (moment(record.dateCreated).format("MM/DD/YYYY") === day) {
    //           bal.push(record.transactionBalance);
    //         } else {
    //           bal.push(0);
    //         }
    //       });

    //       bal = bal[0];
    //       return bal;
    //     })
    //   );
  }, [records, betweenDates]);
  //   useEffect(() => {
  //     let distincts = [];

  //     records.forEach((e) => {
  //       if (!distincts.find((distinct) => distinct === e.distinct)) {
  //         distincts.push(e.distinct);
  //       }
  //     });
  //     setCatName(distincts);
  //   }, [records]);

  const incomeDatas = {
    labels: [...(incomeData?.categoryNames ?? ["1"])],
    datasets: [
      {
        label: "My First Dataset",
        data: [...(incomeData?.totalResult ?? [1])],
        backgroundColor: bgColors,
        hoverOffset: 4,
      },
    ],
  };

  const expenseDatas = {
    labels: [...(expenseData?.categoryNames ?? [])],
    datasets: [
      {
        label: "My First Dataset",
        data: [...(expenseData?.totalResult ?? [])],
        backgroundColor: bgColors,
        hoverOffset: 4,
      },
    ],
  };

  const savingsDatas = {
    labels: [...(savingsData?.categoryNames ?? [])],
    datasets: [
      {
        label: "My First Dataset",
        data: [...(savingsData?.totalResult ?? [])],
        backgroundColor: bgColors,
        hoverOffset: 4,
      },
    ],
  };

  return (
    <Col xs={10} id="page-content-wrapper">
      <h1 className="text-left darkTextColor m-2">Breakdown</h1>
      <Container>
        <Row className="darkTextColor">
          <Col lg={6}>
            <Form.Group controlId="startDate" className="m-0">
              <Form.Label>From:</Form.Label>
              <Form.Control
                type="date"
                value={startDate}
                onChange={(e) => setStartDate(e.target.value)}
              />
            </Form.Group>
          </Col>
          <Col lg={6}>
            <Form.Group controlId="startDate" className="m-0">
              <Form.Label>To:</Form.Label>
              <Form.Control
                type="date"
                value={endDate}
                onChange={(e) => setEndDate(e.target.value)}
              />
            </Form.Group>
          </Col>
        </Row>
      </Container>
      <Row className="text-center mt-3">
        <Col lg={4}>
          <h4>Income</h4>
          <Pie data={incomeDatas} />
        </Col>

        <Col lg={4}>
          <h4>Savings</h4>
          <Pie data={savingsDatas} />
        </Col>

        <Col lg={4}>
          <h4>Expense</h4>
          <Pie data={expenseDatas} />
        </Col>
      </Row>
    </Col>
  );
}
